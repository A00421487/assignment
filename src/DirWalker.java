import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.*;
public class DirWalker {
static FileWriter filewriter;
static CSVPrinter csvprinter;
static CSVFormat csvFileFormat;
//valid record count
static int valid_record_count=0;
//invaid record count
static int invalid_record_count=0;
// list of all valid rows
List<CSVRecord> Validrows;
	public void walk( String path ) throws IOException {

        File root = new File( path );
        File[] list = root.listFiles();
        
        csvFileFormat=CSVFormat.DEFAULT.withRecordSeparator("\n");
        //create object for file write
        SimpleCsvParser csvparser = new SimpleCsvParser();
        String fileName;
        if (list == null) 
        	{
        	return;
        	}

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() );
                System.out.println( "Dir:" + f.getAbsoluteFile() );
            }
            else {
                System.out.println( "File:" + f.getAbsoluteFile() );
            	fileName = f.getAbsoluteFile().toString();
            	Validrows = csvparser.CsvParser(fileName);	
            	csvprinter.printRecords(Validrows);
            }
        }
    }

    public static void main(String[] args) throws IOException{
    	SimpleLogging.loggerfunction();
       // csvprinter = new CSVPrinter(filewriter,csvFileFormat);
    	filewriter=new FileWriter("C:\\Users\\L E N O V O\\workspace\\Assignment\\Final.csv");
        csvprinter = new CSVPrinter(filewriter, CSVFormat.DEFAULT .withHeader("First Name","Last Name","Street Number	","Street", "City","Province","Postal Code","Country","Phone Number","email Address"));      
    	DirWalker dirwalker = new DirWalker();
    	final long startTime = System.currentTimeMillis();
    	dirwalker.walk("C:\\Users\\L E N O V O\\workspace\\Assignment\\Sample Data");
        final long endTime = System.currentTimeMillis();
        SimpleLogging.logger.log(Level.INFO, "Total numberof valid rows: " + DirWalker.valid_record_count);
        SimpleLogging.logger.log(Level.INFO, "Total number of skipped rows: " + DirWalker.invalid_record_count);
        SimpleLogging.logger.log(Level.INFO, "Total execution time is: " + (endTime - startTime) + " ms");
    }
}