import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class SimpleCsvParser {

	public List<CSVRecord> CsvParser(String fileName) {

		Reader in;
		boolean flag=true;		
		List<CSVRecord> Validrows = new ArrayList<CSVRecord>();
		try {
			in = new FileReader(fileName);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
			int row=1;
			for (CSVRecord record : records) {		   
				int length = record.size();
				
			    for(int i=0;i<length;i++ )
			    {
			    	//check for empty record/row
			    	if(record.get(i).isEmpty())
			    	{
			    		flag = false;
			    		SimpleLogging.logger.log(Level.INFO,fileName +" Row: "+ row +"  Column: "+ (i+1));
			    		break;
			    	}
			    	else
			    	{	
			    		flag=true;
			    		// valid rows

			    	}			    	
			    }	
			    
			    if(flag==true&& row!=1)
			    {
			    	DirWalker.valid_record_count=DirWalker.valid_record_count+1;
			    	Validrows.add(record);

			    }
			    else if(flag==false)
			    {
			    	DirWalker.invalid_record_count=DirWalker.invalid_record_count+1;
			    row++;
			    }
			}
			
		} catch ( IOException e) {
			e.printStackTrace();
		}
		return Validrows;	
	}
}